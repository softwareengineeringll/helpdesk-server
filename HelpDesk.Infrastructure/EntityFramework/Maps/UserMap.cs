﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity.ModelConfiguration;
using HelpDesk.Domain.Authentication;

namespace HelpDesk.Infrastructure
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            ToTable("User");
            HasKey(p => p.Id);
            Property(p => p.Id).HasColumnName("Id").IsRequired();
            Property(p => p.Name).HasColumnName("Nombre").HasMaxLength(50).IsRequired();
            Property(p => p.Password).HasColumnName("Clave").HasMaxLength(50).IsRequired();
            Property(p => p.Active).HasColumnName("Activo").IsRequired();
        }
    }
}
