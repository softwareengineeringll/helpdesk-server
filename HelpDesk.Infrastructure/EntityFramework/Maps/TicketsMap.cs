﻿using HelpDesk.Domain.Authentication;
using System.Data.Entity.ModelConfiguration;

namespace HelpDesk.Infrastructure
{
    public class TicketsMap : EntityTypeConfiguration<Ticket>
    {
        public TicketsMap()
        {
            ToTable("Tickets");
            HasKey(p => p.Id);
            Property(p => p.Id).HasColumnName("Id").IsRequired();
            Property(p => p.Asunto).HasColumnName("Asunto").HasMaxLength(50).IsRequired();
            Property(p => p.Mensaje).HasColumnName("Mensaje").HasMaxLength(100).IsRequired();
            Property(p => p.Prioridad).HasColumnName("Prioridad").IsRequired();
            Property(p => p.Estado).HasColumnName("Estado").IsRequired();
            Property(p => p.FechaSolicitud).HasColumnName("FechaSolicitud").IsRequired();
            Property(p => p.FechaRespuesta).HasColumnName("FechaRespuesta").IsRequired();
        }
    }
}
