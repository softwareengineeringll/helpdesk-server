﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity;
using HelpDesk.Domain.Authentication;

namespace HelpDesk.Infrastructure
{
    public class HelpDeskDBContext : DbContext
    {
        public HelpDeskDBContext() : base("HelpDesk") // nombre Cadena de conexion
        {
            Database.SetInitializer<HelpDeskDBContext>(null);
        }

        public DbSet<User> User { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TicketsMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}
