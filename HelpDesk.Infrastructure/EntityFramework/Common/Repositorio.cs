﻿using HelpDesk.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace HelpDesk.Infrastructure
{
    public class Repositorio<T> : IRepositorio<T> where T : class
    {
        private readonly DbContext _dbContext;
        public Repositorio(DbContext dbcontext)
        {
            _dbContext = dbcontext;
        }
        public void Agregar(T entidad)
        {
            _dbContext.Set<T>().Add(entidad);
        }

        public void Guardar()
        {
            _dbContext.SaveChanges();
        }

        public T Obtener(Func<T, bool> condicion)
        {
            return _dbContext.Set<T>().FirstOrDefault(condicion);
        }

        public IEnumerable<T> ObtenerTodos()
        {
            return _dbContext.Set<T>().ToList();
        }
    }
    
}
