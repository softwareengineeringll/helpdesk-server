﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpDesk.Domain.Authentication
{
    public class AuthenticationDomainService
    {
        public bool SonCredencialesValidas(User usuario, string nombre, string contraseña)
        {
            if (usuario == null)
            {
                return false;
            }

            if (nombre == null)
            {
                return false;
            }

            if (contraseña == null)
            {
                return false;
            }

            if (usuario.Name != nombre)
            {
                return false;
            }

            if (usuario.Password != contraseña)
            {
                return false;
            }

            if (usuario.Active == false)
            {
                return false;
            }

            return true;
        }

        
    }
}
