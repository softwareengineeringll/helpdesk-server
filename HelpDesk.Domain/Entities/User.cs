﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpDesk.Domain.Authentication
{
    public class User
    {
        public User()
        {
            Active = true;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
    }
}
