﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpDesk.Domain.Authentication
{
    public class Ticket
    {
        public int Id { get; set; }

        public string Asunto { get; set; }

        public string Mensaje { get; set; }

        public int Prioridad { get; set; }

        public int Estado { get; set; }

        public DateTime FechaSolicitud { get; set; }

        public DateTime FechaRespuesta { get; set; }


    }
}
