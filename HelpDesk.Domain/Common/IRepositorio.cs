﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpDesk.Domain
{
    public interface IRepositorio<T> where T : class
    {
        void Agregar(T entidad);

        T Obtener(Func<T, bool> condicion);

        IEnumerable<T> ObtenerTodos();

        void Guardar();
    
    }
}
