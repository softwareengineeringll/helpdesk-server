﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpDesk.Application.Dtos
{
    public class UserDto
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
