﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HelpDesk.Application.Dtos
{
    public class TicketDto
    {
        public string Id { get; set; }
        public string Asunto { get; set; }
        public string Mensajes { get; set; }
        public string Prioridad { get; set; }
        public string Estado { get; set; }
        public string FechaSolicitud { get; set; }
        public string FechaRespuesta { get; set; }
    }
}
